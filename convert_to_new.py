# stdlib
import argparse
import html
import re
import textwrap

# in-tree deps
import debug


def parse_args():
	parser = argparse.ArgumentParser(description="Convert a Jekyll formatted TWIF to Hugo format.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("filename", help="Jekyll-formatted MD file to convert.")
	options = parser.parse_args()
	return options

def rdbg(before, after):
	debug.dprint("Replacing {!r} with {!r}".format(before, after))
	return after

def replace(haystack, needle, replacement):
	try:
		i = haystack.index(needle)
		return haystack[:i] + replacement + haystack[i + len(needle):]
	except ValueError:
		return haystack

#RE_BASEPOSTURL  = re.compile("\\{\\{\n?[ \t]*site\\.baseurl\n?[ \t]*\\}\\}(\\{\n?\\{[^}]+\\}\\})")
RE_BASEURL      = re.compile("\\{\\{\n?[ \t]*site\\.baseurl\n?[ \t]*\\}\\}([^)\"]*)([\")])")
RE_JEKYLLMACRO  = re.compile("\\{\\{\n?[ \t]*([^<\n}]+?)\n?[ \t]*\\}\\}")
RE_JEKYLLMACRO2 = re.compile("\\{%\n?[ \t]*([^<\n}]+?)\n?[ \t]*%\\}")
RE_PILL         = re.compile("^page\\.(featured|major|fdroid)$")
RE_GETVALUE     = re.compile("^page\\.([A-Za-z0-9_]+)$")
RE_POSTURL      = re.compile("^post_url\\b\n?[ \t]*([A-Za-z0-9_.-]+)$")

RE_REFLOW1	= re.compile("(---\n)")
RE_REFLOW2	= re.compile("(\n\\*[ \t][^\n]*)\n[ \t]+([^\n])")
RE_REFLOW3	= re.compile("(\n[^#][^\n]*)\n([^\n])")

def reflow(text):
	empty, d1, header, d2, body = RE_REFLOW1.split(text, 2)

	prev_body = ""
	while prev_body != body:
		prev_body = body
		body = RE_REFLOW2.sub("\\1 \\2", body)
		#body = RE_REFLOW3.sub("\\1 \\2", body)

	return "".join((empty, d1, header, d2, body))

def replace_baseurl(matchobj):
	body = matchobj.group(1)
	debug.dprint("Baseurl:", matchobj.group(0))
	if body.startswith(("/assets", "/posts")):
		return rdbg(matchobj.group(0), "{{{{< absURL \"{}\" >}}}}{}".format(html.escape(body[1:], True), matchobj.group(2)))
	if body.startswith("/"):
		return rdbg(matchobj.group(0), "https://f-droid.org{}{}".format(html.escape(body, True), matchobj.group(2)))
	return rdbg(matchobj.group(0), "https://f-droid.org/{}{}".format(html.escape(body, True), matchobj.group(2)))

def replace_jekyll_macro(matchobj):
	body = matchobj.group(1)
	debug.dprint(repr(body))

	m = RE_PILL.search(body)
	if m:
		return rdbg(matchobj.group(0), "{{{{< pill {} >}}}}".format(m.group(1).lower()))

	m = RE_GETVALUE.search(body)
	if m:
		return rdbg(matchobj.group(0), "{{{{% getvalue {} %}}}}".format(m.group(1).lower()))

	debug.dprint("Unrecognized jekyll macro: {!r}".format(body))
	debug.dprint(matchobj.group(0))
	raise Exception("Unrecognized jekyll macro: {!r}".format(body))

def replace_jekyll2_macro(matchobj):
	body = matchobj.group(1)
	debug.dprint(repr(body))

	m = RE_POSTURL.search(body)
	if m:
		return rdbg(matchobj.group(0), "{}".format(html.escape(m.group(1), True)))
		#return rdbg(matchobj.group(0), "{{{{< relref \"{}\" >}}}}".format(html.escape(m.group(1), True)))

	debug.dprint("Unrecognized jekyll macro: {!r}".format(matchobj.group(0)))
	debug.dprint(matchobj.group(0))
	raise Exception("Unrecognized jekyll macro: {!r}".format(matchobj.group(0)))

if __name__ == '__main__':
	opts = parse_args()

	m = re.search("(^|/)([0-9]{4}-[01][0-9]-[0-9][0-9])-", opts.filename)
	date = m.group(2)

	debug.dprint("Stage 1: Read")
	with open(opts.filename, "r", encoding='UTF-8') as f:
		jekyllfile = f.read()

	debug.dprint("Stage 2: TWIF-specific substitutions")
	footer = textwrap.dedent("""\
	### Tips and Feedback

	Do you have important app updates we should write about? Send in your tips via [Mastodon](https://joinmastodon.org)! Send them to {{ page.mastodonAccount }} and remember to tag with {{ page.twifTag }}. Or use the {{ page.twifThread }} on the forum. The deadline to the next TWIF is **Thursday** 12:00 UTC.

	General feedback can also be sent via Mastodon, or, if you'd like to have a live chat, you can find us in **#fdroid** on [Freenode](https://freenode.net), on Matrix via {{ page.matrixRoom }} or on [Telegram]({{ page.telegramRoom }}). All of these spaces are bridged together, so the choice is yours. You can also join us on the **[forum]({{ page.forum }})**.
	""")

	jekyllfile = replace(jekyllfile, footer, "{{< twif_footer >}}\n")

	deleteheaders = textwrap.dedent("""\
	layout: post
	authorWebsite: "https://open.source.coffee"
	fdroid:
	featured:
	featuredv1:
	major:
	""").strip().splitlines()

	h = 0
	hugofile = []
	for line in jekyllfile.splitlines(True):
		if line.rstrip() == "---":
			if h == 1:
				line = "date: {}\n{}".format(date, line)
			h += 1
		if h == 1:
			if line.strip() == "":
				line = ""
			else:
				for dh in deleteheaders:
					if line.startswith(dh):
						line = ""
						break

		if "page.date | date: \"%V, %G\"" in line:
			line = "{{< twif_edition >}}\n"

		if line.rstrip() == "<!--more-->":
			line = "<!--more-->{{<x>}} Dear translators. Please leave this line unchanged. Thank you. {{</x>}}\n"

		if line.rstrip() == """**[F-Droid](https://f-droid.org/)** is a [repository](https://f-droid.org/packages/) of verified [free and open source](https://en.wikipedia.org/wiki/Free_and_open-source_software) Android apps, a **[client](https://f-droid.org/app/org.fdroid.fdroid)** to access it, as well as a whole "app store kit", providing all the tools needed to set up and run an app store. It is a community-run free software project developed by a wide range of contributors. This is their story this past week.""":
			line = "{{< twif_preamble >}}\n"

		hugofile.append(line)

	hugofile = "".join(hugofile)

	#debug.dprint("Stage 2.5: Reflow")
	#hugofile = reflow(hugofile)

	debug.dprint("Stage 3: Jekyll percent-macros")
	hugofile = RE_JEKYLLMACRO2.sub(replace_jekyll2_macro, hugofile)

	debug.dprint("Stage 4: Rewrite site.baseurl expressions")
	hugofile = RE_BASEURL.sub(replace_baseurl, hugofile)
	debug.dprint(hugofile)

	debug.dprint("Stage 5: Jekyll accolade-macros")
	hugofile = RE_JEKYLLMACRO.sub(replace_jekyll_macro, hugofile)

	print(hugofile, end="")
